package ru.kolesnikov.tm.api.service;

import ru.kolesnikov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
